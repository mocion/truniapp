import "react-native-gesture-handler";
import React from "react";
import Main from "./src/stacks/main";
import { NavigationContainer } from "@react-navigation/native";
import { QueryClient, QueryClientProvider } from "react-query";
import { AuthContextProvider } from "./src/context/authContext";
import { ContextEventProvider } from "./src/context/defaultContext";
// import { getToken } from "./src/helpers/AppPermissions";

const queryClient = new QueryClient();

export default function App() {
  // getToken();
  return (
    <QueryClientProvider client={queryClient}>
      <AuthContextProvider>
        <ContextEventProvider>
          <NavigationContainer>
            <Main />
          </NavigationContainer>
        </ContextEventProvider>
      </AuthContextProvider>
    </QueryClientProvider>
  );
}
