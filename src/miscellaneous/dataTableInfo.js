import React, { useState, useContext, useEffect } from "react";
import { ScrollView, View } from "react-native";
import { Button, IconButton } from "react-native-paper";
import { DataTable } from "react-native-paper";
import { ContextEvent } from "../context/defaultContext";

export const DataTableInfo = () => {
  let { eventsList, userConfig } = useContext(ContextEvent);
  const [employeeList, setEmployeeList] = useState(["sin filtro"]);
  const [asignationList, setAsignationList] = useState(["sin filtro"]);
  const [employeeCounter, setEmployeeCounter] = useState(0);
  const [asignationCounter, setAsignationCounter] = useState(0);

  useEffect(() => {
    if (eventsList) {
      const le = ["sin filtro"];
      const la = ["sin filtro"];
      eventsList.map((event) => {
        const ne = event.employee;
        const na = event.asignation;
        !le.includes(ne) && le.push(ne);
        !la.includes(na) && la.push(na);
      });
      setEmployeeList(le);
      setAsignationList(la);
    }
  }, [eventsList]);

  const counter = (employee) => {
    if (employee) {
      setAsignationCounter(0);
      employeeCounter === employeeList.length - 1
        ? setEmployeeCounter(0)
        : setEmployeeCounter(employeeCounter + 1);
    } else {
      setEmployeeCounter(0);
      asignationCounter === asignationList.length - 1
        ? setAsignationCounter(0)
        : setAsignationCounter(asignationCounter + 1);
    }
  };

  const tableItem = (event) => {
    const startMinute = event.start.getMinutes();
    const endMinute = event.end.getMinutes();
    const startDate = `${event.start.getHours()}:0${startMinute}`;
    const endDate = `${event.end.getHours()}:0${endMinute}`;
    if (event.email === userConfig.email || userConfig.rol === "admin") {
      return (
        <DataTable.Row>
          {asignationCounter === 0 && (
            <DataTable.Cell>{event.asignation}</DataTable.Cell>
          )}
          {employeeCounter === 0 && (
            <DataTable.Cell>{event.employee}</DataTable.Cell>
          )}
          <DataTable.Cell numeric>{startDate}</DataTable.Cell>
          <DataTable.Cell numeric>{endDate}</DataTable.Cell>
        </DataTable.Row>
      );
    }
  };

  console.log(employeeList, asignationList);
  return (
    <ScrollView>
      {userConfig && userConfig.rol === "admin" && (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "flex-start",
          }}
        >
          <Button onPress={() => counter(true)}>
            {employeeList[employeeCounter]}
          </Button>
          <Button onPress={() => counter(false)}>
            {asignationList[asignationCounter]}
          </Button>
          <IconButton
            icon="restart"
            size={20}
            onPress={() => {
              setEmployeeCounter(0);
              setAsignationCounter(0);
            }}
          />
        </View>
      )}
      <DataTable>
        <DataTable.Header>
          {asignationCounter === 0 && (
            <DataTable.Title>Espacio</DataTable.Title>
          )}
          {employeeCounter === 0 && <DataTable.Title>Empleado</DataTable.Title>}
          <DataTable.Title numeric>Entrada</DataTable.Title>
          <DataTable.Title numeric>Salida</DataTable.Title>
        </DataTable.Header>

        {eventsList &&
          userConfig &&
          eventsList.length > 0 &&
          eventsList.map((event) => {
            if (employeeCounter === 0 && asignationCounter === 0) {
              return tableItem(event);
            } else if (employeeCounter > 0 && event.employee === employeeList[employeeCounter]) {
              return tableItem(event);
            } else if (asignationCounter > 0 && event.asignation === asignationList[asignationCounter]) {
              return tableItem(event);
            }
          })}
      </DataTable>
    </ScrollView>
  );
};
