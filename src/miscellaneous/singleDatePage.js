import React, { useContext } from "react";
import DatePicker from "react-native-datepicker";
import { ContextEvent } from "../context/defaultContext";

export const SingleDatePage = () => {
  let { selectedDate, changeSelectedDate } = useContext(ContextEvent);

  return (
    <DatePicker
      style={{ width: 200 }}
      date={selectedDate}
      mode="date"
      placeholder="select date"
      format="YYYY-MM-DD"
      minDate="2021-08-01"
      maxDate="2021-08-15"
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={{
        dateIcon: {
          position: "absolute",
          left: 0,
          top: 4,
          marginLeft: 0,
        },
        dateInput: {
          marginLeft: 36,
        },
        // ... You can check the source to find the other keys.
      }}
      onDateChange={(date) => changeSelectedDate(date)}
    />
  );
};
