import React, { useState, useContext } from "react";
import { Pressable, View } from "react-native";
import { Avatar, Button } from "react-native-paper";
import { AuthContext } from "../context/authContext";
import { ContextEvent } from "../context/defaultContext";

const MenuUser = () => {
  const [visible, setVisible] = useState(false);
  const changeVisibility = () => setVisible(!visible);
  let { signOut } = useContext(AuthContext);
  let { userConfig } = useContext(ContextEvent);

  return (
    <View style={styles.container}>
      {visible && (
        <Button visible={visible} mode="contained" onPress={() => signOut()}>
          Log Out
        </Button>
      )}
      <Pressable onPress={() => changeVisibility()}>
        {userConfig ? (
          <Avatar.Text
            size={36}
            label={
              userConfig.name.slice(0, 1)
            }
            style={{ backgroundColor: "#eee" }}
          />
        ) : (
          <Avatar.Text
            size={36}
            label={"GU"}
            style={{ backgroundColor: "#eee" }}
          />
        )}
      </Pressable>
    </View>
  );
};

export default MenuUser;

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    width: "80%",
  },
};
