import React from "react";
import { Avatar, Card } from "react-native-paper";

export const LoadingScreen = () => {
  const LeftContent = (props) => <Avatar.Icon {...props} icon="loading" />;
  return (
    <Card>
      <Card.Title
        title="Please wait..."
        subtitle="We're loading this component"
        left={LeftContent}
      />
    </Card>
  );
};
