import React, { useContext } from "react";
import { SafeAreaView, Image, Text, View, ScrollView } from "react-native";
import { Appbar } from "react-native-paper";
import QRCode from "react-native-qrcode-svg";
import { ContextEvent } from "../context/defaultContext";

const Profile = ({ navigation }) => {
  const { infoUser } = useContext(ContextEvent);
  console.log(infoUser);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <ScrollView>
        <QRCode
          //QR code value
          value={"https://evius.co/events"}
          size={250}
          color="black"
          backgroundColor="white"
        />
        {Object.keys(infoUser).map((key, index) => {
          return infoUser[key].slice(0, 4) === "http" ? (
            <View style={styles.info_container} key={key}>
              <Text style={styles.text_labels} key={index}>
                {key.toUpperCase()}:
              </Text>
              <Image
                key={key}
                source={{ uri: infoUser[key] }}
                style={styles.image_info}
              />
            </View>
          ) : key != "password" ? (
            <View style={styles.info_container} key={key}>
              <Text style={styles.text_labels} key={index}>
                {key.toUpperCase()}:
              </Text>
              <Text style={{ fontSize: 14 }} key={key}>
                {key}
              </Text>
            </View>
          ) : null;
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Profile;

const styles = {
  image_info: {
    minWidth: 150,
    minHeight: 150,
    // resizeMode: 'contain',
  },
  text_labels: {
    fontWeight: "bold",
  },
  info_container: {
    marginHorizontal: 10,
    marginTop: 10,
    padding: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
};
