import React from "react";
import { SafeAreaView } from "react-native";
import { Appbar } from "react-native-paper";
import { DataTableInfo } from "../miscellaneous/dataTableInfo";
import { SingleDatePage } from "../miscellaneous/singleDatePage";

const Turns = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <SingleDatePage />
      <DataTableInfo />
    </SafeAreaView>
  );
};

export default Turns;
