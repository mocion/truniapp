import React, { useContext } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Directory } from "../helpers/config";
import { useQuery } from "react-query";
import { AuthContext } from "../context/authContext";
import { LoadingScreen } from "../miscellaneous/LoadingScreen";
import { getEventInfo } from "../context/defaultContext";

const Drawer = createDrawerNavigator();
// aqui se añade los nuevos screen

const Main = () => {
  let { user } = useContext(AuthContext);
  const { isLoading, isError, data } = useQuery("EVENTINFO", getEventInfo);
  if (isLoading) return <LoadingScreen />;
  if (isError) return null;
  // el 'name' de cada stack, equivale a 'section' por cada 'itemsMenu'
  return (
    <Drawer.Navigator
      edgeWidth={100}
      drawerStyle={{
        backgroundColor: `${data.data.styles.toolbarDefaultBg}`,
      }}
      drawerContentOptions={{
        activeTintColor: `${data.data.styles.color_tab_agenda}`,
      }}
    >
      {!user ? (
        <>
          <Drawer.Screen name="Login" component={Directory.login} />
          <Drawer.Screen name="Register" component={Directory.register} />
        </>
      ) : (
        <>
          <Drawer.Screen name="Home" component={Directory.home} />
          <Drawer.Screen name="Turnos" component={Directory.turns} />
          <Drawer.Screen name="Perfil" component={Directory.profile} />
          {Object.keys(data.data.itemsMenu).map((key) => {
            return (
              <Drawer.Screen
                key={key}
                name={data.data.itemsMenu[`${key}`].name}
                component={Directory[key]}
              />
            );
          })}
        </>
      )}
    </Drawer.Navigator>
  );
};

export default Main;
