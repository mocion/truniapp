import React, { useContext } from "react";
import { SafeAreaView, ScrollView, Image } from "react-native";
import { homeMenu, Icons } from "../helpers/config";
import { Button, Appbar } from "react-native-paper";
import MenuUser from "../miscellaneous/menuUser";
import { LoadingScreen } from "../miscellaneous/LoadingScreen";
import { ContextEvent } from "../context/defaultContext";

const Home = ({ navigation }) => {
  const { infoEvent } = useContext(ContextEvent);
  if (!infoEvent) return <LoadingScreen />;
  const data = infoEvent.data.data;
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.Action
          icon="menu"
          onPress={() => {
            navigation.openDrawer();
          }}
        />
        <MenuUser />
      </Appbar.Header>
      <ScrollView style={{ backgroundColor: data.styles.containerBgColor }}>
        <Image
          style={homeMenu.image}
          source={{ uri: `${data.picture}` }}
        ></Image>
        <Button
          icon={"calendar"}
          style={homeMenu.btn}
          onPress={() => {
            navigation.navigate("Turnos");
          }}
        >
          Turnos
        </Button>
        <Button
          icon={"human-greeting"}
          style={homeMenu.btn}
          onPress={() => {
            navigation.navigate("Perfil");
          }}
        >
          Perfil
        </Button>
        {Object.keys(data.itemsMenu).map((key) => {
          return (
            <Button
              key={key}
              icon={Icons[data.itemsMenu[key].icon]}
              style={homeMenu.btn}
              onPress={() => {
                navigation.navigate(data.itemsMenu[`${key}`].name);
              }}
            >
              {data.itemsMenu[`${key}`].name}
            </Button>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
