import React from "react";
import { Text, SafeAreaView } from "react-native";
import { Appbar } from "react-native-paper";

const document = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <Text>Document</Text>
    </SafeAreaView>
  );
};

export default document;
