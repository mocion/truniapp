import React from "react";
import { Text, SafeAreaView } from "react-native";
import { Appbar } from "react-native-paper";

const Companies = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <Text>Companies</Text>
    </SafeAreaView>
  );
};

export default Companies;
