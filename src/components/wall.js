import React from "react";
import { SafeAreaView, Text } from "react-native";
import { Appbar } from "react-native-paper";

const Wall = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <Text>Wall</Text>
    </SafeAreaView>
  );
};

export default Wall;
