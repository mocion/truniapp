import React, { useContext } from "react";
import { Text, SafeAreaView } from "react-native";
import { Button } from "react-native-paper";
import { Appbar } from "react-native-paper";
import { LoadingScreen } from "../miscellaneous/LoadingScreen";
import { ContextEvent } from "../context/defaultContext";

const Event = ({ navigation }) => {
  const { infoEvent } = useContext(ContextEvent);
  if (!infoEvent) return <LoadingScreen />;
  const data = infoEvent.data.data;
  return (
    <SafeAreaView
      style={{ flex: 1, backgroundColor: data.styles.containerBgColor }}
    >
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <Text>Event</Text>
      <Button icon="camera">Press me</Button>
    </SafeAreaView>
  );
};

export default Event;
