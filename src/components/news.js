import React from "react";
import { SafeAreaView, Text } from "react-native";
import { Appbar } from "react-native-paper";

const News = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <Text>News</Text>
    </SafeAreaView>
  );
};

export default News;
