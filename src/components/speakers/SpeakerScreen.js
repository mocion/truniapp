import React, { useContext } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { Appbar } from "react-native-paper";
import { LoadingScreen } from "../../miscellaneous/LoadingScreen";
import { ContextEvent } from "../../context/defaultContext";
import SpeakerItem from "./SpeakerItem";

const SpeakerScreen = ({ navigation }) => {
  const { infoHost } = useContext(ContextEvent);
  if (!infoHost) return <LoadingScreen />;
  return (
    <SafeAreaView style={{flex: 1}}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
      </Appbar.Header>
      <ScrollView>
        {infoHost.data.data.data.map((speaker, index) => {
          return <SpeakerItem key={index} speaker={speaker} />;
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default SpeakerScreen;
