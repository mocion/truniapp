import React from "react";
import RenderHtml from "react-native-render-html";
import { WebView } from "react-native-webview";
import { useWindowDimensions } from "react-native";
import { Card, Title, Paragraph, Avatar } from "react-native-paper";

const SpeakerItem = ({speaker}) => {
    const { width } = useWindowDimensions();
  return (
    <Card elevation={2} style={{ marginBottom: 8 }}>
      <Card.Content style={{ alignItems: "center" }}>
        <Avatar.Image
          size={128}
          source={{uri:
            speaker.image ||
            "https://e7.pngegg.com/pngimages/505/761/png-clipart-login-computer-icons-avatar-icon-monochrome-black.png"
          }}
        />
        <Title>{speaker.name}</Title>
        <Paragraph>{speaker.profession}</Paragraph>
        <RenderHtml contentWidth={width} source={{ html: speaker.description }} />
        <WebView
          originWhitelist={["*"]}
          source={{ html: `${speaker.description}` }}
        />
      </Card.Content>
    </Card>
  );
};

export default SpeakerItem;
