import React, { useState } from "react";
import { Card, Title, Paragraph } from "react-native-paper";
import { ScheduleDetail } from "./ScheduleDetail";
import moment from "moment";

export function ScheduleItem({ activity }) {
  const [detailCard, setDetailCard] = useState(false);
  const dateStart = moment(activity.datetime_start)
  return (
    <Card
      elevation={2}
      onPress={() => setDetailCard(!detailCard)}
      style={{ marginTop: 10, marginHorizontal: 10 }}
    >
      <Card.Content>
        <Paragraph>Hora: {dateStart.format("HH:mm")}</Paragraph>
        <Title>{activity.name}</Title>
        <Paragraph>{activity.subtitle}</Paragraph>
      </Card.Content>
      {detailCard && <ScheduleDetail activity={activity} />}
      <Card.Actions />
    </Card>
  );
}
