import React, { useContext } from "react";
import { Appbar } from "react-native-paper";
import { SafeAreaView, ScrollView } from "react-native";
import { SingleDatePage } from "../../miscellaneous/singleDatePage";
import { Text } from "react-native-paper";
import { ScheduleItem } from "./ScheduleItem";
import { ContextEvent } from "../../context/defaultContext";

export default function ScheduleScreen({ navigation }) {
  const { infoActivities: activities } = useContext(ContextEvent);
  return (
    <SafeAreaView>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      </Appbar.Header>
      <SingleDatePage />
      <ScrollView>
        {activities.length > 0 ? (
          activities.map((activity, index) => (
            <ScheduleItem key={index} activity={activity} />
          ))
        ) : (
          <Text> No hay actividades aún para esta fecha </Text>
        )}
      </ScrollView>
    </SafeAreaView>
  );
}
