import React from "react";
import RenderHtml from "react-native-render-html";
import { useWindowDimensions } from "react-native";
import { Card, Paragraph } from "react-native-paper";

export const ScheduleDetail = ({ activity }) => {
  const { width } = useWindowDimensions();
  const { description, space, image, activity_categories } = activity;
  console.log(activity);
  return (
    <Card.Content>
      <RenderHtml contentWidth={width} source={{ html: description }} />
      {image && <Card.Cover source={{ uri: image }} />}
      {space && <Paragraph>Espacio: {space.name}</Paragraph>}
      {activity_categories.length > 0 &&
        activity_categories.map((category) => {
          return <Paragraph>{category.name}</Paragraph>;
        })}
    </Card.Content>
  );
};
