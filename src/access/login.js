import React, { useState, useContext } from "react";
import { View } from "react-native";
import { TextInput, Card, Button } from "react-native-paper";
import { AuthContext } from "../context/authContext";

export const Login = ({navigation}) => {
  let { signInUser } = useContext(AuthContext);
  const [text, setText] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <Card>
        <TextInput
          label="Email"
          value={text}
          onChangeText={(text) => setText(text)}
        />
        <TextInput
          label="Password"
          value={password}
          secureTextEntry
          onChangeText={(text) => setPassword(text)}
          right={<TextInput.Icon name="eye" />}
        />
        <Button mode="contained" onPress={() => signInUser(text.toLowerCase(), password)}>
          Ingresar
        </Button>
        <Button onPress={() => navigation.navigate("Register")}>Registrar</Button>
      </Card>
    </View>
  );
};

var styles = {
  container: {
    justifyContent: "center",
    height: "100%"
  },
};
