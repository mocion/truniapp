import React, { useState, useContext } from "react";
import { View } from "react-native";
import { TextInput, Card, Button, Snackbar } from "react-native-paper";
import { AuthContext } from "../context/authContext";

const Register = ({ navigation }) => {
  let { createUser } = useContext(AuthContext);
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [visible, setVisible] = useState(false);
  const [text, setText] = useState("");

  const onToggleSnackBar = () => setVisible(!visible);
  const onDismissSnackBar = () => setVisible(false);

  const createNewUser = () => {
    if (email !== "" && name !== "") {
      if (password.length >= 6) {
        const data = {
          properties: {
            names: name,
            apellidos: "lastname",
            email: email.toLowerCase(),
            celular: "123456789",
            nacionalidad: "123456789",
            domicilio: "Calle falsa 123",
            medicacion: "Ninguna",
            enfermedades: "Ninguna",
            alergias: "Ninguna",
            rh: "A+",
            pasaporte: "passport",
            visa: "visa",
            identificacion: "id",
            password: password,
          },
        };
        createUser(data);
      } else {
        setText("Por favor introduce una contraseña más larga");
        onToggleSnackBar();
      }
    } else {
      setText("Por favor completa tus datos");
      onToggleSnackBar();
    }
  };

  return (
    <View style={styles.container}>
      <Card>
        <TextInput
          label="Nombre"
          value={name}
          onChangeText={(text) => setName(text)}
        />
        <TextInput
          label="Email"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <TextInput
          label="Password"
          value={password}
          secureTextEntry
          onChangeText={(text) => setPassword(text)}
          right={<TextInput.Icon name="eye" />}
        />
        <Button onPress={() => navigation.navigate("Login")}>Ingresar</Button>
        <Button mode="contained" onPress={() => createNewUser()}>
          Registrar
        </Button>
        <Snackbar
          visible={visible}
          onDismiss={onDismissSnackBar}
          action={{
            label: "Okey",
            onPress: () => {
              // Do something
            },
          }}
        >
          {text}
        </Snackbar>
      </Card>
    </View>
  );
};

var styles = {
  container: {
    justifyContent: "center",
    height: "100%",
  },
  snackBar: {
    flex: 1,
    justifyContent: "space-between",
  },
};

export default Register;
