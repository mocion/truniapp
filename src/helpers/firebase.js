import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var firebaseConfig = {
  apiKey: "AIzaSyAGhnaznb-fY_ysdz2UhoLouvIOYBqO4vg",
  authDomain: "truni-21c5b.firebaseapp.com",
  projectId: "truni-21c5b",
  storageBucket: "truni-21c5b.appspot.com",
  messagingSenderId: "756379773468",
  appId: "1:756379773468:web:353232b3a7a504d89bcdd9",
  measurementId: "G-14N7XY7K2Z",
};

firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();
export const auth = firebase.auth();
