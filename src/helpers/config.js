import { StyleSheet, Dimensions } from "react-native";
import {Login} from "../access/login";
import Register from "../access/Register";
import Home from "../components/home";
import ScheduleScreen from "../components/schedule/ScheduleScreen";
import Activity from "../components/activity";
import Document from "../components/document";
import SpeakerScreen from "../components/speakers/SpeakerScreen";
import Wall from "../components/wall";
import Faqs from "../components/faqs";
import Section from "../components/section";
import News from "../components/news";
import Event from "../components/event";
import Certificate from "../components/certificate";
import Survey from "../components/survey";
import Networking from "../components/networking";
import Companies from "../components/companies";
import Interviews from "../components/interviews";
import Trophies from "../components/trophies";
import Informative from "../components/informative";
import Partners from "../components/partners";
import Turns from "../truniComponents/Turns";
import Profile from "../truniComponents/Profile";

export const homeMenu = StyleSheet.create({
  btn: {
    padding: 12,
  },
  image: {
    height: 250,
    width: Dimensions.get("window").width,
  },
});

export const Directory = {
  login: Login,
  register: Register,
  home: Home,
  turns: Turns,
  profile: Profile,
  interviews: Interviews,
  agenda: ScheduleScreen,
  my_sesions: Activity,
  speakers: SpeakerScreen,
  documents: Document,
  wall: Wall,
  faqs: Faqs,
  my_section: Section,
  informativeSection: News,
  evento: Event,
  certs: Certificate,
  survey: Survey,
  networking: Networking,
  companies: Companies,
  trophies: Trophies,
  informativeSection1: Informative,
  partners: Partners,
  tickets: Partners,
};

export const Icons = {
  ReadOutlined: "account-clock-outline",
  FileDoneOutlined: "file-check-outline",
  ApartmentOutlined: "office-building",
  FolderOutlined: "folder-multiple-outline",
  CalendarOutlined: "calendar",
  QuestionOutlined: "comment-question",
  UserOutlined: "human-greeting",
  LoginOutlined: "login",
  EnterOutlined: "folder-heart-outline",
  TeamOutlined: "human-male-male",
  LaptopOutlined: "laptop",
  DeploymentUnitOutlined: "nature-people",
  AudioOutlined: "music-note-outline",
  FileUnknownOutlined: "file-question-outline",
  CreditCardOutlined: "credit-card-check-outline",
  TrophyOutlined: "trophy",
}