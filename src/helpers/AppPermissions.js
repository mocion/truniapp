// import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'
import * as Notifications from 'expo-notifications'

export const getToken = async () => {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
    if (status !== "granted"){
        return;
    }
    const token = await Notifications.getExpoPushTokenAsync()
    console.log("token",token)
    return token;
}