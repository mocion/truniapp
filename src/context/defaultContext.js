import axios from "axios";
import React, { useState, useEffect, useContext } from "react";
import { useQuery } from "react-query";
import { db } from "../helpers/firebase";
import { AuthContext } from "../context/authContext";

const EVIUS_URL = "https://api.evius.co/api/";
const API_ID_EVENT = "60e4bd9c8e8ab015a0099463";
const URL_COMPOUND = `${EVIUS_URL}events/${API_ID_EVENT}`;

export const getEventInfo = () => axios(URL_COMPOUND);

export const ContextEventProvider = ({ children }) => {
  let { user } = useContext(AuthContext);

  // states generales
  const [userConfig, setUserConfig] = useState();
  const [eventsList, setEventsList] = useState();
  const [selectedDate, setSelectedDate] = useState("2021-08-02");
  const [infoActivities, setInfoActivities] = useState([]);
  const [infoUser, setInfoUser] = useState({});

  // peticiones a Evius
  const getInfo = (request) => axios(request);
  const getInfoHost = () => axios(URL_COMPOUND + "/host");

  // useQueries las funciones no reciben params :(
  const options = { staleTime: Infinity };
  const infoEvent = useQuery("EVENTINFO", getEventInfo, options);
  const infoHost = useQuery("EVENTSPEAKERS", getInfoHost, options);

  useEffect(() => {
    if (user) {
      const filterEmail = `?filtered=[{"field":"properties.email","value":"${user.email}"}]`;
      db.collection(`employees`)
        .where("email", "==", user.email)
        .onSnapshot((querySnapshot) => {
          querySnapshot.forEach((element) => {
            setUserConfig(element.data());
          });
        });
      getInfo(URL_COMPOUND + "/eventusers" + filterEmail).then((res) =>
        setInfoUser(res.data.data[0].properties)
      );
    }
  }, [user]);

  useEffect(() => {
    const filterDate = `?filtered=[{"field":"datetime_start","value":"${selectedDate}","comparator":"like"}]`;
    getEventList();
    getInfo(URL_COMPOUND + "/activities" + filterDate).then((res) =>
      setInfoActivities(res.data.data)
    );
  }, [selectedDate]);

  function getEventList() {
    const minDate = new Date(selectedDate);
    var maxDate = new Date(selectedDate);
    maxDate = maxDate.setDate(maxDate.getDate() + 1);
    db.collection("events")
      .where("start", ">=", minDate)
      .where("start", "<=", new Date(maxDate))
      .onSnapshot((querySnapshot) => {
        var docsList = [];
        querySnapshot.forEach((doc) => {
          var item = doc.data();
          item.email;
          item.start = item.start.toDate();
          item.end = item.end.toDate();
          docsList.push(item);
        });
        setEventsList(docsList);
      });
  }

  const changeSelectedDate = (date) => {
    setSelectedDate(date);
  };

  const value = {
    userConfig,
    eventsList,
    selectedDate,
    infoEvent,
    infoUser,
    infoHost,
    infoActivities,
    restartSearch: (item) => {
      setFilterSelected(item ? item : defaultFilterItem);
    },
    changeSelectedDate,
  };

  return (
    <ContextEvent.Provider value={value}>{children}</ContextEvent.Provider>
  );
};

export const ContextEvent = React.createContext({});
