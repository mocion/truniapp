import axios from "axios";
import React, { useEffect, useState } from "react";
import { auth, db } from "../helpers/firebase";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const AuthContext = React.createContext({});

export function AuthContextProvider({ children }) {
  const [user, setUser] = useState(null);

  useEffect(() => {
    AsyncStorage.getItem("user").then((res) => {
      setUser(JSON.parse(res));
    });
  }, []);

  const signInUser = (email, password) => {
    auth
      .signInWithEmailAndPassword(email, password)
      .then((credential) => {
        setUser(credential.user);
        AsyncStorage.setItem("user", JSON.stringify(credential.user));
      })
      .catch((error) => console.log(error.code, error.message));
  };

  const createUser = (data) => {
    const { email, name, password } = data;
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((credential) => {
        // crear usuario en Evius
        const params = "/adduserwithemailvalidation/?no_send_mail=true";
        axios.post(URL_COMPOUND + params, data);
        // almacenar el usuario
        setUser(credential.user);
        AsyncStorage.setItem("user", JSON.stringify(credential.user));
        // crear nuevo registro en firebase TODO: validar si es necesario
        const newUser = createNewUser(email, name);
        db.collection(`employees`).doc(`${newUser.id}`).set(newUser);
      })
      .catch((error) => console.log(error.code, error.message));
  };

  const signOut = () => {
    auth
      .signOut()
      .then(() => {
        setUser(null);
        AsyncStorage.removeItem("user");
      })
      .catch((error) => console.log(error.code, error.message));
  };

  const value = {
    user,
    signInUser,
    createUser,
    signOut,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

const createNewUser = (email, name) => {
  const id =
    Date.now() +
    Math.floor(Math.random() * Math.floor(Math.random() * Date.now()));
  let hexColor = "#";
  for (let i = 0; i < 3; i++)
    color += (
      "0" + Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)
    ).slice(-2);
  return { hexColor, email, id, name, rol:"user" };
};
